export default {
  genPhotoItemsList: function (minCount, maxCount, isAlbum) {
    var items = []

    for (var i = 0; i < this.random(maxCount, minCount); i++) {
      items.push(this.genPhotoItem(i, isAlbum))
    }

    return items
  },
  genPhotoItem: function (id, isAlbum = false) {
    var imgSrc = 'https://picsum.photos/300/300?image=' + (Math.floor(Math.random() * 100) + 1)

    var item = {}

    item.id = id
    item.img = imgSrc

    if (isAlbum) {
      item.name = this.genLoremIpsum(50)
      item.photoIds = this.genIds(50)
      item.albumsIds = this.genIds(20)
    }

    return item
  },
  genIds: function (maxIds, maxValue = 500) {
    const num = this.random(maxIds)

    var setIds = new Set([])

    for (var i = 0; i < num; i++) {
      setIds.add(this.random(maxValue))
    }

    return Array.from(setIds)
  },
  genLoremIpsum: function (maxValue, minValue = 10) {
    const lorem = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, ' +
    'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut ' +
    'enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi' +
    ' ut aliquip ex ea commodo consequat.'

    return lorem.substring(0, this.random(maxValue, minValue))
  },
  random: function (maxValue, minValue = 0) {
    return Math.max(Math.floor(Math.random() * maxValue), minValue)
  }
}
