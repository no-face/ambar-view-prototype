import Vue from 'vue'
import Router from 'vue-router'
import VuetifyApp from '@/components/VuetifyApp'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: VuetifyApp
    }
  ]
})
